## Webservice Portal

This repository contains the source registry of all tools and services shown on https://webservices.cls.ru.nl . This uses the software metadata harvesting pipeline produced in CLARIAH, see https://github.com/CLARIAH/tool-discovery .

Add new tools by doing a merge request here. The metadata for the tools will be automatically harvested and presented.  


